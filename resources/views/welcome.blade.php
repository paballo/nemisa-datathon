
<!DOCTYPE html>

 <html lang="en">

<head>

     <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--theme colors -->
    <meta name="theme-color" content="" />
    <meta name="apple-mobile-web-app-status-bar-style" content="">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!--Basic meta info -->
    <meta name="keywords" content="Nemisa , Datathon , Geekulcha">
    <meta name="author" content="Paballo Monareng" />
    <meta name="description" content="NEMISA is hosting a National Datathon on 17-19 November 2023 as part of their National Skills Development Plan and Digital Skills Strategy.">

    <!--OpenGraph meta -->
    <meta property="og:description" content="NEMISA is hosting a National Datathon on 17-19 November 2023 as part of their National Skills Development Plan and Digital Skills Strategy."/>
    <meta property="og:title" content="Nemisa Datathon" />
    <meta property="og:image" content="https://www.nemisa.co.za/wp-content/uploads/2020/06/Nemisa-logo-.png"/>
    <meta property="og:url" content="" />

    <!--meta for twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:creator" content="">
    <meta name="twitter:title" content="Nemisa Datathon">
    <meta name="twitter:image" content="https://www.nemisa.co.za/wp-content/uploads/2020/06/Nemisa-logo-.png">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="NEMISA is hosting a National Datathon on 17-19 November 2023 as part of their National Skills Development Plan and Digital Skills Strategy.">

<title>Nemisa Datathon</title>

<!-- Stylesheets -->

<link href="https://expert-themes.com/html/eventrox/css/bootstrap.css" rel="stylesheet">

<link href="https://expert-themes.com/html/eventrox/css/style.css" rel="stylesheet">

<link href="https://expert-themes.com/html/eventrox/css/responsive.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="https://kit.fontawesome.com/818a67e1eb.js" crossorigin="anonymous"></script>

<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

</head>

<style>
   body {
      scroll-behavior: smooth !important;
   }
   blockquote {
  background: #f9f9f9;
  border-left: 10px solid #ccc;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
}
blockquote:before {
  color: #ccc;
  content: "";
  font-size: 4em;
  line-height: 0.1em;
  margin-right: 0.25em;
  vertical-align: -0.4em;
}
blockquote p {
  display: inline;
  font-size: 16px;
  line-height: 28px;
  color: #888888;
}
</style>

<body>

<div class="page-wrapper">
    <!-- Main Header-->
    <header class="main-header">
        <div class="main-box">
        	<div class="auto-container clearfix">
            	<div class="logo-box">
                	<div class="logo" style="margin-top: 20px;color:#ec167f;font-size:17px">#NemisaDatathon2023</div>
                </div>
                <!--Nav Box-->
                <div class="nav-outer clearfix">
                    <!--Mobile Navigation Toggler-->
                    <div class="mobile-nav-toggler"><span class="icon flaticon-menu"></span></div>
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md navbar-light">
                        <div class="navbar-header">
                            <!-- Togg le Button -->      
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon flaticon-menu-button"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse clearfix" id="navbarSupportedContent">
                            <ul class="navigation clearfix">
                                <li><a href="#">Home</a> </li>
                                <li ><a href="#about">About</a></li>
                                <li ><a href="#event-information">Event </a></li>
                                <li ><a href="#partners">Partners</a></li>
                            </ul>
                        </div>
                    </nav>
                    <div class="outer-box">
                        <!-- Button Box -->
                        <div class="btn-box">
                            <a href="#register" class="theme-btn btn-style-one"><span class="btn-title">Register Now</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile Menu  -->
        <div class="mobile-menu">
            <div class="menu-backdrop"></div>
            <div class="close-btn"><span class="icon flaticon-cancel-1"></span></div>
            <!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header-->
            <nav class="menu-box">
                <div class="nav-logostyle=" style="20px;color:#cb5a5e;font-size:17px" >#NemisaDatathon2023</div>
                <ul class="navigation clearfix"><!--Keep This Empty / Menu will come through Javascript--></ul>
            </nav>
        </div><!-- End Mobile Menu -->
    </header>
    <!-- Banner Section -->
    <section class="banner-section">
        <div class="banner-carousel owl-carousel owl-theme">
            <!-- Slide Item -->
            <div class="slide-item" style="background-image: url(https://live.staticflickr.com/65535/50911125816_4254572672_o_d.jpg);">
                <div class="auto-container">
                    <div class="content-box">
                        <h2> Nemisa <br> Datathon 2023</h2>
                        <h5>#BuldingACapable4IRArmy</h5>
                             <br>
                        <div class="btn-box"><a href="#register" class="theme-btn btn-style-two"><span class="btn-title">Register Now</span></a></div>
                    </div>  
                </div>
            </div>
        </div>
    </section>

    <!-- Coming Soon -->
    <section class="coming-soon-section">
        <div class="auto-container">
            <div class="outer-box">
                <div class="time-counter"><div class="time-countdown clearfix" data-countdown="11/17/2023"></div></div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section class="about-section" id="about">
        <div class="anim-icons full-width">
            <span class="icon icon-circle-blue wow fadeIn"></span>
            <span class="icon icon-dots wow fadeInleft"></span>
            <span class="icon icon-circle-1 wow zoomIn"></span>
        </div>

        <div class="auto-container"  >
            <div class="row">
                <div class="content-column col-lg-6 col-md-12 col-sm-12">
                    <div class="inner-column">
                        <div class="sec-title">
                            <span class="title">ABOUT EVENT</span>
                            <h2>Welcome to the Nemisa Datathon 2023</h2>
                            <div class="text">NEMISA is hosting a National Datathon on 17-19 November 2023 as part of their National Skills Development Plan and Digital Skills Strategy. </div>
                             <blockquote> <p>The art and  of deriving meaning from data is known as. The use of tools to derive meaning from data is where the art of lies. As a result, multiple solutions are revealed by the variety of usage, which reflects the diversity of focus, mandate, and context. Overall, has enormous potential to create a positive societal impact, but it is important to ensure that it is used ethically and responsibly, with a focus on creating solutions that are equitable and inclusive.</p></blockquote>   
                            <div class="text">The NEMISA datathon 2023 aims to bring together various stakeholders to explore the intersection of digital skills, economic uncertainty, and unemployment. The datathon participants will analyze large datasets and create innovative solutions that can help policymakers and practitioners to address the challenges and opportunities related to this field.</div>
                        </div>

                    </div>
                </div>
                <!-- Image Column -->
                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="image-box">
                        <figure class="image wow fadeIn"><img src="https://live.staticflickr.com/65535/50911216132_ef8903cc78_o_d.jpg" alt="" style="width: 450px;height: 445px"></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--End About Section -->
    <section class="why-choose-us" id="event-information">
        <div class="auto-container">
            <div class="row">
                <div class="content-column col-lg-6 col-md-12 col-sm-12 order-2">
                    <div class="inner-column">
                        <div class="sec-title">
                            <span class="title">JOIN THE EVENT</span>
                            <h2>Why Choose Nemisa Datathon?</h2>
                            <div class="text">Attending this Datathon offers several benefits, including the opportunity to network with top professionals in the industry. By interacting with these experts, you can establish valuable connections that may lead to free mentorship or referrals to promising career opportunities within the field.</div>
                            <br>
                            <h2>Event Information</h2>
                            <br>
                            <br>
                            <div class="container" style="position: relative; background-color: #ffffff;border: 1px solid #eeeeee;box-shadow: 0 30px 40px rgba(0,0,0,0.10);transition: all 500ms ease;overflow: hidden;padding: 40px 20px;">
                                <div class="letter-border" style="height: 10px;width: 100%;background: repeating-linear-gradient(-45deg, #cb5a5e, #cb5a5e 8px, transparent 8px, transparent 18px);"></div>
                                <div class="letter-title" style="margin-top: 10px;margin-left: 5px;height: 10px;width: 40%;background: #cb5a5e;"></div>
                                <div class="letter-context" style="margin-top: 10px;margin-left: 5px;height: 10px;width: 20%;background: #cb5a5e;content: 'ram avtar';"></div>
                                <div class="py-4"></div>
                                <div class="row" style="margin-left: 30px">
                                  <div class="col-sm list-style-one" >
                                    <i class="fa fa-calendar" style="position: relative;display: inline-block;font-size: 15px;line-height: 1em;transition: all 400ms ease;"> <span>Date</span></i>
                                  </div>
                                  <div class="col-sm list-style-one">
                                    <div >
                                        <div style="position: relative;display: block;font-size: 12px;line-height: 24px;color: #888888;cursor: default;font-weight: bold;">
                                            17-19 November 2023
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <br>
                                <div class="row" style="margin-left: 30px">
                                    <div class="col-sm list-style-one">
                                      <i class="fa fa-clock" style="position: relative;display: inline-block;font-size: 15px;line-height: 1em;transition: all 400ms ease;"> <span >Time</span></i>
                                    </div>
                                    <div class="col-sm list-style-one" >
                                        <div style="position: relative;display: block;font-size: 12px;line-height: 24px;color: #888888;font-weight: bold;;cursor: default;">
                                             08:00 am - 17:00 pm
                                        </div>
                                    </div>
                                  </div>
                                  <br>
                                  <div class="row" style="margin-left: 30px">
                                    <div class="col-sm list-style-one">
                                      <i class="fa fa-map-marker" style="position: relative;display: inline-block;font-size: 15px;line-height: 1em;transition: all 400ms ease;"> <span>Venue</span></i>
                                    </div>
                                    <div class="col-sm list-style-one">
                                        <div style="position: relative;display: block;font-size: 12px;line-height: 24px;color: #888888;font-weight: bold;cursor: default;">
                                            Physical : Unisa Pretoria Mian Campus (Kgorong Building)
                                            <br>
                                            Virtual : Zoom
                                        </div>
                                    </div>
                                  </div>
                                  <br>
                                  <div class="row" style="margin-left: 30px">
                                    <div class="col-sm list-style-one" >
                                      <i class="fa fa-bullseye" style="position: relative;display: inline-block;font-size: 15px;line-height: 1em;transition: all 400ms ease;"> <span>Target Participants</span></i>
                                    </div>
                                    <div class="col-sm">
                                        <ul class="list-style-one">
                                          
                                            <i class="fa fa-check" > &nbsp;&nbsp;<span> Software / Hardware Developers</span></i>
                                             <br>
                                            <i class="fa fa-check" > &nbsp;&nbsp;<span>Data Analyst</span></i>
                                             <br>
                                            <i class="fa fa-check" > &nbsp;&nbsp;<span>Entrepreneurs</span></i>

                
                                        </ul>
                                    </div>
                                  </div>
                               <div class="letter-context" style="margin-top: 10px;margin-le: 200px;height: 10px;width: 20%;background: #cb5a5e;content: 'ram avtar';"></div>
                               <br>
                               <div class="letter-title" style="margin-top: 10px;margin-left: 320px;height: 10px;width: 40%;background: #cb5a5e;"></div>
                               <br>
                               <div class="letter-border" style="height: 10px;width: 100%;background: repeating-linear-gradient(-45deg, #cb5a5e, #cb5a5e 8px, transparent 8px, transparent 18px);"></div>
                              </div>
                           </div>
                    </div>
                </div>

                <div class="image-column col-lg-6 col-md-12 col-sm-12">
                    <div class="image-box">
                        <figure class="image"><img src="https://expert-themes.com/html/eventrox/images/background/3.jpg" alt=""></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Features Section Two -->

    <section class="features-section-two">
      <div class="auto-container">
          <div class="anim-icons">   
              <span class="icon twist-line-1 wow zoomIn"></span>
              <span class="icon twist-line-2 wow zoomIn" data-wow-delay="1s"></span>
              <span class="icon twist-line-3 wow zoomIn" data-wow-delay="2s"></span>
          </div>

          <div class="row">
              <div class="title-block col-lg-4 col-md-12 col-sm-12 wow fadeInUp">
                  <div class="inner-box">
                      <div class="sec-title">
                          <span class="title">Focus</span>
                          <h2>Themes</h2>
                      </div>
                  </div>
              </div>

              <div class="feature-block-two col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                <div class="inner-box" style="height: 27rem">
                    <div class="icon-box"><i class="icon fa fa-lightbulb-o"></i></div>
                      <h4>Digital Skills and labour market trends</h4>
                    <div class="text">Participants will analyze large datasets related to the job market , including data on employment, job vancancies, and skill requirement.</div>
                    <div class="text">The aim is to explore the impact of digital skills on the job market and identify trends and patterns  that can inform policy and practice.</div>
                </div>
              </div>

              <div class="feature-block-two col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="400ms">
                  <div class="inner-box" style="height: 27rem">
                      <div class="icon-box"><span class="icon fa fa-users"></span></div>
                        <h4>Economic uncertainty and employment opportunities</h4>
                      <div class="text">Participants will analyze datasets related to economic and political uncertainty, including data on GDP growth, inflation and political instability. The aim is to explore the relationship between economic uncertainty and unemployment opportunities for individuals with digital skills.</div>
                  </div>
              </div>
                <div class="feature-block-two col-lg-4 col-md-6 col-sm-12 wow fadeInUp" data-wow-delay="800ms"> </div>
              <div class="feature-block-two col-lg-4 col-md-6 col-sm-12 wow fadeInUp">
                    <div class="inner-box" style="height: 27rem">
                         <div class="icon-box"><span class="icon fa fa-graduation-cap"></span></div>
                         <h4>Policies and strategies for promoting digital and data skills</h4>
                         <div class="text">Participants will analyze datasets related to policies and strategies for promoting digital skills , including data on government spending on education and training , and the effectiveness of different approaches. The aim is to identify best practices andd recommendations for policymakers and practitioners. </div>
                    </div>
            </div>
        </div>
          </div>
      </div>

  </section>
    <!--Clients Section-->

    <section class="clients-section" id="partners">
        <div class="anim-icons">
            <span class="icon icon-dots-3 wow zoomIn"></span>
            <span class="icon icon-circle-blue wow zoomIn"></span>
        </div>
        <div class="auto-container">
            <div class="sec-title">
                <span class="title">Clients</span>
                <h2>Offcial Partners</h2>
            </div>
            <div class="Partners-outer">
                <div class="row">
                    <div class="client-block col-lg-3 col-md-6 col-sm-12">
                        <figure class="image-box" style="height: 5rem"><img src="https://www.nemisa.co.za/wp-content/uploads/2020/06/Nemisa-logo-.png" alt="" style="margin-top: 15px"></figure>
                    </div>
                    <div class="client-block col-lg-3 col-md-6 col-sm-12">
                        <figure class="image-box" style="height: 5rem"><img src="https://cisp.cachefly.net/assets/articles/images/resized/0001067113_resized_unisalogo1022.jpg" alt="" ></figure>
                    </div>
                    <div class="client-block col-lg-3 col-md-6 col-sm-12">
                        <figure class="image-box"  style="height: 5rem"><img src="https://www.geekulcha.dev/logo_white.png" alt=""  style="filter: invert(1); margin-top: 60px;"></figure>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--End Clients Section-->

    <section class="faq-section">
      <div class="auto-container">
          <div class="sec-title">
              <span class="title">FAQ'S</span>
              <h2>General <span>Questions</span></h2>
          </div>
          <div class="row clearfix">
              <div class="content-column col-lg-6 col-md-12 col-sm-12">
                  <div class="inner-column">
                      <ul class="accordion-box">
                          <li class="accordion block active-block wow fadeInUp">
                              <div class="acc-btn active"><div class="icon-outer"><span class="icon icon-plus fa fa-angle-down"></span> </div>If I can't find a team, can I work alone?</div>
                              <div class="acc-content  current">
                                  <div class="content">
                                      <div class="text">No, a team is required to be in team of at least 3 people</div>
                                  </div>
                              </div>
                          </li>
                          <li class="accordion block  wow fadeInUp">
                              <div class="acc-btn "><div class="icon-outer"><span class="icon icon-plus fa fa-angle-down"></span> </div>Can we change the design?</div>
                              <div class="acc-content">
                                  <div class="content">
                                      <div class="text">Yes but only one solution will be selected to be worked on during the Datathon</div>
                                  </div>
                              </div>
                          </li>
                      </ul>
                  </div>
              </div>
              <div class="content-column col-lg-6 col-md-12 col-sm-12">
                  <div class="inner-column">
                      <ul class="accordion-box">
                          <li class="accordion block wow fadeInUp">
                              <div class="acc-btn "><div class="icon-outer"><span class="icon icon-plus fa fa-angle-down"></span> </div>Who does the Intellectual Property belong to?</div>
                              <div class="acc-content">
                                  <div class="content">
                                      <div class="text">The solution belongs to your team at the end of the Datathon</div>
                                  </div>
                              </div>
                          </li>
                          <li class="accordion block wow fadeInUp">
                              <div class="acc-btn"><div class="icon-outer"><span class="icon icon-plus fa fa-angle-down"></span> </div>Will NEMISA incubate our solution at the end of the Datathon?</div>
                              <div class="acc-content">
                                  <div class="content">
                                      <div class="text">With prize money, a team is expected to continue working on their solution with assistance from the host hubs of the Datathon.</div>
                                  </div>
                              </div>
                          </li>
                      </ul>
                  </div>
              </div>  
          </div>
      </div>
  </section>

<footer>

</footer>
</div>

<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-angle-double-up"></span></div>

<script src="https://expert-themes.com/html/eventrox/js/jquery.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/popper.min.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/bootstrap.min.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/jquery-ui.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/jquery.fancybox.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/appear.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/owl.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/jquery.countdown.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/wow.js"></script>
<script src="https://expert-themes.com/html/eventrox/js/script.js"></script>
</body>
</html>